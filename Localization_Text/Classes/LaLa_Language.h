#ifndef __LALA_LANGUAGE_H__
#define __LALA_LANGUAGE_H__

#include "cocos2d.h"
#include "json\rapidjson.h"
#include "json\document.h"
#include "json\prettywriter.h"
#include "json\writer.h"
#include "json\stringbuffer.h"
#include "json\filereadstream.h"

using namespace cocos2d;
namespace lala{
	void setCurrentLanguage(char* lang);
	const char* getCurrentLanguge();
	std::string getString(const char* Key);
}
#endif // __LALA_LANGUAGE_H__