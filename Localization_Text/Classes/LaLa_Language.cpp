#include "LaLa_Language.h"
using namespace cocos2d;

bool doc_lang_inited = false;
rapidjson::Document doc_lang;
char* curLangugeCode = nullptr;
void lala::setCurrentLanguage(char* lang){
	curLangugeCode = lang;
}
const char* lala::getCurrentLanguge(){
	if (curLangugeCode == nullptr)
		return Application::getInstance()->getCurrentLanguageCode();
	else return curLangugeCode;
}
std::string lala::getString(const char* Key){
	if (!doc_lang_inited){
		doc_lang_inited = true;
		std::string filename = "lang/";
		filename = filename + getCurrentLanguge();
		filename = filename + ".lang";
		if (FileUtils::getInstance()->isFileExist(filename)){
			Data jsonRaw = FileUtils::getInstance()->getDataFromFile(filename);
			char* stream = reinterpret_cast<char*>(jsonRaw.getBytes());
			std::string str(stream, jsonRaw.getSize());
			rapidjson::StringStream s = rapidjson::StringStream(str.c_str());
			doc_lang.ParseStream(s);
		}
		else if (FileUtils::getInstance()->isFileExist("lang/default.lang")){
			Data jsonRaw = FileUtils::getInstance()->getDataFromFile("lang/default.lang");
			char* stream = reinterpret_cast<char*>(jsonRaw.getBytes());
			std::string str(stream, jsonRaw.getSize());
			rapidjson::StringStream s = rapidjson::StringStream(str.c_str());
			doc_lang.ParseStream(s);
		}
		else {
			return Key;
		}
	}
	if (doc_lang.HasMember(Key)){
		return doc_lang[Key].GetString();
	}
	else {
		return Key;
	}
}