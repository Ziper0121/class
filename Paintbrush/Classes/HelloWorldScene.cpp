#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}
	background = Sprite::create("dson.jpg");
	background->setPosition(Vec2(480, 320));
	addChild(background);

	button = Button::create("mario1.png");
	button->setPosition(Vec2(900, 500));
	addChild(button);

	button->addClickEventListener(CC_CALLBACK_1(HelloWorld::Button_Touch, this));

	button2 = Button::create("mario1.png");
	button2->setPosition(Vec2(900, 400));
	addChild(button2);

	button2->addClickEventListener(CC_CALLBACK_1(HelloWorld::Button_Touch2, this));

	button3 = Button::create("mario1.png");
	button3->setPosition(Vec2(900, 300));
	addChild(button3);

	button3->addClickEventListener(CC_CALLBACK_1(HelloWorld::Button_Touch3, this));

	button4 = Button::create("mario1.png");
	button4->setPosition(Vec2(900, 200));
	addChild(button4);

	button4->addClickEventListener(CC_CALLBACK_1(HelloWorld::Button_Touch4, this));

	clipping = ClippingNode::create();
	node = Sprite::create();
	clipping->setStencil(node);

	Line = DrawNode::create();
	clipping->addChild(Line);
	clipping->setInverted(true);
	clipping->setAlphaThreshold(0.01f);

	addChild(clipping);
	
	auto touch_on = EventListenerTouchOneByOne::create();
	touch_on->onTouchBegan = std::bind(&HelloWorld::mouse_Began, this, std::placeholders::_1, std::placeholders::_2);
	touch_on->onTouchMoved = std::bind(&HelloWorld::mouse_Moved, this, std::placeholders::_1, std::placeholders::_2);
	touch_on->onTouchEnded = std::bind(&HelloWorld::mouse_End, this, std::placeholders::_1, std::placeholders::_2);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touch_on, this);
	
	
	//addchild는 부모의 클래스에 상속받는 다 라는 뜻이다?

    return true;
}

bool HelloWorld::mouse_Began(Touch* tc, Event* et){
	if (Eraser) {
		Sprite* sprite = Sprite::create("masker.png");
		sprite->setPosition(tc->getLocation());
		node->addChild(sprite);
	}
	else
		P1 = tc->getLocation();
	return true;
}
void HelloWorld::mouse_Moved(Touch* tc, Event* et){
	if (Eraser) {
		Sprite* sprite2 = Sprite::create("masker.png");
		sprite2->setPosition(tc->getLocation());
		node->addChild(sprite2);
	}
	else{
		Line->drawLine(P1, tc->getLocation(), Color4F(R, G, B, 1));
		P1 = tc->getLocation();
	}
}
void HelloWorld::mouse_End(Touch* tc, Event* et){
	if (Eraser) {
		Sprite* sprite3 = Sprite::create("masker.png");
		sprite3->setPosition(tc->getLocation());
		node->addChild(sprite3);
	}
	else {
		Line->drawLine(P1, tc->getLocation(), Color4F(R, G, B, 1));
	}
}
void HelloWorld::Button_Touch(Ref* rf){
	if (Eraser) Eraser = false;
	else Eraser = true;
}
void HelloWorld::Button_Touch2(cocos2d::Ref* rf){
	if (R == 0)
		R = 1.0f;
	else
		R = 0.0f;
}
void HelloWorld::Button_Touch3(cocos2d::Ref* rf){
	if (G == 0)
		G = 1.0f;
	else
		G = 0.0f;
}
void HelloWorld::Button_Touch4(cocos2d::Ref* rf){
	if (B == 0)
		B = 1.0f;
	else
		B = 0.0f;
}