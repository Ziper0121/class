#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
#include "ui/CocosGUI.h"

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	cocos2d::DrawNode* Line;
	cocos2d::Vec2 P1;

	cocos2d::Sprite* background;
	cocos2d::ui::Button* button;
	cocos2d::ui::Button* button2;
	cocos2d::ui::Button* button3;
	cocos2d::ui::Button* button4;

	cocos2d::ClippingNode* clipping;

	Node* node;

	bool Eraser = false;

	void Button_Touch(cocos2d::Ref* rf);
	void Button_Touch2(cocos2d::Ref* rf);
	void Button_Touch3(cocos2d::Ref* rf);
	void Button_Touch4(cocos2d::Ref* rf);
	float R = 0.0f;
	float G = 0.0f;
	float B = 0.0f;

	bool mouse_Began(cocos2d::Touch* tc, cocos2d::Event* et);
	void mouse_Moved(cocos2d::Touch* tc, cocos2d::Event* et);
	void mouse_End(cocos2d::Touch* tc, cocos2d::Event* et);
};

#endif // __HELLOWORLD_SCENE_H__
