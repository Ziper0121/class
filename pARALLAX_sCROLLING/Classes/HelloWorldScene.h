#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	cocos2d::ParallaxNode* parallax;
	cocos2d::Sprite* n100x100;
	cocos2d::Sprite* n50x50;

	cocos2d::Label* console;

	bool Keys[128];

	void parallaxTestCase2();
	void KeyPressed(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
	void KeyReleased(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
	void KeyLogic(float dt);
};

#endif // __HELLOWORLD_SCENE_H__
