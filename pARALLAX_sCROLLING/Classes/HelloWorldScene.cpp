#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	HelloWorld::parallaxTestCase2();
    return true;
}
void HelloWorld::parallaxTestCase2(){
	for (int i = 0; i < 128; i++){
		Keys[i] = false;
	}
	parallax = ParallaxNode::create();
	n100x100 = Sprite::create("100x100.png");
	n50x50 = Sprite::create("50x50.png");
	console = Label::createWithSystemFont("", "Arial.ttf", 20.0f);
	console->setAlignment(TextHAlignment::LEFT);
	console->setPosition(480, 100);
	addChild(console);

	parallax->addChild(n100x100, -2, Vec2(0.5f, 0.5f), Vec2(0, 0));
	parallax->addChild(n50x50, -1, Vec2(1.0f, 1.0f), Vec2(0, 0));
	parallax->setPosition(Director::getInstance()->getOpenGLView()->getDesignResolutionSize() / 2);
	auto keybd = EventListenerKeyboard::create();
	keybd->onKeyPressed = std::bind(&HelloWorld::KeyPressed, this, std::placeholders::_1, std::placeholders::_2);
	keybd->onKeyReleased = std::bind(&HelloWorld::KeyReleased, this, std::placeholders::_1, std::placeholders::_2);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(keybd, this);
	addChild(parallax);
	schedule(schedule_selector(HelloWorld::KeyLogic));
}
void HelloWorld::KeyPressed(EventKeyboard::KeyCode code, Event* e){
	Keys[static_cast<size_t>(code)] = true;
}
void HelloWorld::KeyReleased(EventKeyboard::KeyCode code, Event* e){
	Keys[static_cast<size_t>(code)] = false;
}
void HelloWorld::KeyLogic(float dt){
	Vec2 offset;
	if (Keys[static_cast<size_t>(EventKeyboard::KeyCode::KEY_LEFT_ARROW)]) offset.x = -1.0f;
	if (Keys[static_cast<size_t>(EventKeyboard::KeyCode::KEY_RIGHT_ARROW)]) offset.x = 1.0f;
	if (Keys[static_cast<size_t>(EventKeyboard::KeyCode::KEY_UP_ARROW)]) offset.y = 1.0f;
	if (Keys[static_cast<size_t>(EventKeyboard::KeyCode::KEY_DOWN_ARROW)]) offset.y = -1.0f;
	parallax->setPosition(parallax->getPosition() + offset);

	std::stringstream ss;
	//��� ��ǥ
	ss << "100x100 pos x: " << n100x100->getPosition().x << ", y: " << n100x100->getPosition().y;
	ss << "\n";
	ss << "50x50 pos x: " << n50x50->getPosition().x << ", y: " << n50x50->getPosition().y;
	ss << "\n";

	//������ǥ

	ss << "100x100 real pos x:" << parallax->convertToWorldSpace(n100x100->getPosition()).x;
	ss << ", y" << parallax->convertToWorldSpace(n100x100->getPosition()).y;
	ss << "\n";
	ss << "50x50 real pos x:" << parallax->convertToWorldSpace(n50x50->getPosition()).x;
	ss << ", y" << parallax->convertToWorldSpace(n50x50->getPosition()).y;
	console->setString(ss.str());
}