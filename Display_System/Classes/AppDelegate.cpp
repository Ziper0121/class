#include "AppDelegate.h"
#include "HelloWorldScene.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {

	Size designRes = Size(800, 600);

	Size smallRes = Size(640, 480);
	Size mediumRes = Size(800, 600);
	Size largeRes = Size(1024, 768);

    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
        glview = GLViewImpl::createWithRect("SBSLecture", Rect(0, 0, 800, 600));
#else 
		glview = GLViewImpl::create("SBSLecture");
#endif
        director->setOpenGLView(glview);
    }

    director->getOpenGLView()->setDesignResolutionSize(designRes.width,designRes.height,
		ResolutionPolicy::NO_BORDER);
	Size frameSize = glview->getFrameSize();

	float contentScaleFactor;
	if (frameSize.height > mediumRes.height){
		FileUtils::getInstance()->addSearchPath("res/large");
		contentScaleFactor = MIN(largeRes.height / designRes.height,
			largeRes.width / designRes.width);
	}
	else if (frameSize.height > smallRes.height){
		FileUtils::getInstance()->addSearchPath("res/medium");
		contentScaleFactor = MIN(mediumRes.height / designRes.height,
			mediumRes.width / designRes.width);
	}
	else{
		FileUtils::getInstance()->addSearchPath("res/small");
		contentScaleFactor = MIN(smallRes.height / designRes.height,
			smallRes.width / designRes.width);

	}
	director->setContentScaleFactor(contentScaleFactor);
	FileUtils::getInstance()->addSearchPath("res");

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
