#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"


USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	CCLOG("Hello");
	mario = Sprite::create("BirdBody1.png");
	Sprite* bird = Sprite::create("BirdHand1.png");
	addChild(bird);
	addChild(mario);
	bird->setPosition(Vec2(100, 100));
	mario->setPosition(Vec2(100, 100));
	return true;
}
void HelloWorld::runAcition(float dt){
	Vec2 mario_positon = mario->getPosition();
	//0번은 좌우 +
	//1번은 우측 대각선 - 
	//2번은 좌측 대각선 +
	/*
	시간 제한으로 이동하는 방법
	지정위치가 넘을 경우 하지만 
	if가 각이 있는 면부터 돌아야한다.
	*/
	Vec2 two[3];
	for (int i = 0; i < 3; i++){
		two[i].x = 2 * cos((double)i* PI / PI);
		two[i].y = 2 * sin((double)i* PI / PI);
	}
	if (Moving_state == 0){
		if (mario_positon.x > 300){
			Moving_state = 1;
		}
		mario_positon += two[0];
	}
	else{
		if (mario_positon.x < 200){
			Moving_state = 0;
			mario_positon = Vec2(200,200);
		}
		else if (Moving_state == 2){
			mario_positon -= two[1];
		}
		else if (mario_positon.y < 300){
			mario_positon += two[2];
		}
		if (mario_positon.y > 300){
			Moving_state = 2;
		}
	}
	mario->setPosition(mario_positon);
}