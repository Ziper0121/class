#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
	cocos2d::Sprite* sprPanda;
	cocos2d::TMXTiledMap* Map;
	cocos2d::TMXLayer* walls;
	cocos2d::TMXLayer* grounds;
	bool keys[128];

	cocos2d::Animation* MovePanda;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	void onKeyboad(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);
	void offKeyboad(cocos2d::EventKeyboard::KeyCode, cocos2d::Event*);

	void Muve(float dt);
};

#endif // __HELLOWORLD_SCENE_H__
