#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;


Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	//게임의	블럭의 아이디에 1을 더해주어야 한다. 글로벌 아이디
	Map = TMXTiledMap::create("Map.tmx");
	addChild(Map);

	walls = Map->getLayer("Walls");
	grounds = Map->getLayer("Grounds");

	sprPanda = Sprite::create("Panda1.png");
	Animation* anim = Animation::create();
	anim->addSpriteFrameWithFile("panda1.png");
	anim->addSpriteFrameWithFile("panda2.png");
	anim->setDelayPerUnit(0.5f);
	Animate* animate = Animate::create(anim);
	sprPanda->runAction(RepeatForever::create(animate));

	MovePanda = Animation::create();
	MovePanda->setDelayPerUnit(0.5f);

	//현재 posStart는 Tile의 왼쪽 하단 기준 좌표가 나온다.
	Vec2 posStart = grounds->getPositionAt(Vec2(5, 19)) + Vec2(16, 16);
	sprPanda->setPosition(posStart);

	addChild(sprPanda);

	for (int i = 0; i < 128; i++) keys[i] = false;

	auto Keybd = EventListenerKeyboard::create();
	Keybd->onKeyPressed = std::bind(&HelloWorld::onKeyboad, this, std::placeholders::_1, std::placeholders::_2);
	Keybd->onKeyReleased = std::bind(&HelloWorld::offKeyboad, this, std::placeholders::_1, std::placeholders::_2);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(Keybd, this);

	schedule(schedule_selector(HelloWorld::Muve));

    return true;
}

void HelloWorld::onKeyboad(cocos2d::EventKeyboard::KeyCode Kc, cocos2d::Event* et){
	keys[static_cast<int>(Kc)] = true;
}
void HelloWorld::offKeyboad(cocos2d::EventKeyboard::KeyCode Kc, cocos2d::Event* et){
	keys[static_cast<int>(Kc)] = false;
}

void HelloWorld::Muve(float dt){
	Vec2 pos = sprPanda->getPosition();

	float offset = 100.0f* dt;
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_RIGHT_ARROW)]) pos.x += offset;
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_LEFT_ARROW)]) pos.x -= offset;
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_UP_ARROW)])pos.y += offset;
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_DOWN_ARROW)]) pos.y -= offset;

	//이동할 지점 판다의 Tile 상의 위치 구하기
	Vec2 coord = pos - Vec2(16, 16);
	float tileWidth = Map->getTileSize().width;
	float tileHeight = Map->getTileSize().height;
	coord.x /= tileWidth;
	coord.y /= tileHeight;
	
	//내림값 구하기 [올림 값 : ceil(...)]
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_RIGHT_ARROW)])coord.x = ceil(coord.x);
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_LEFT_ARROW)])coord.x = floor(coord.x);
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_UP_ARROW)])coord.y = ceil(coord.y);
	if (keys[static_cast<int>(EventKeyboard::KeyCode::KEY_DOWN_ARROW)])coord.y = floor(coord.y);
	// coord.y 의 좌표를 역으로 바꿔줌.
	coord.y = (Map->getMapSize().height - 1) - coord.y;

	int groundGid = grounds->getTileGIDAt(coord);
	CCLOG("x : %f , y:%f, groundGid:%d", coord.x, coord.y, groundGid);
	// 960,640
	if (groundGid == 108)
		sprPanda->setPosition(pos);
	Vec2 Relatively = getPosition() + pos;
	if (Relatively.y > 450)
		setPosition(getPosition() + Vec2(0, -2.0f));
	if (Relatively.y < 200)
		setPosition(getPosition() + Vec2(0, 2.0f));
	if (Relatively.x > 750)
		setPosition(getPosition() + Vec2(-2.0f, 0));
	if (Relatively.x < 200)
		setPosition(getPosition() + Vec2(2.0f,0));

}