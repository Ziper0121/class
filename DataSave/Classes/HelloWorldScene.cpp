#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);
    return scene;
}

bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	Button* saveData = Button::create("bin.png");
	saveData->setPosition(Vec2(200,320));
	saveData->setTitleText("Save ValuMap");
	saveData->setTitleColor(Color3B(1, 0, 0));
	saveData->addClickEventListener([](Ref* rf)->void{
		auto fileutil = FileUtils::getInstance();
		std::string path = fileutil->getWritablePath();
		CCLOG("Writable path : %s", path.c_str());

		std::string saveFileFullpath = path + "myData.xml";
		CCLOG("Save File Full- Path: %s", saveFileFullpath);

		// == Prepare Save file 
		ValueVector myItems = ValueVector();
		myItems.push_back(Value("1"));

		ValueMap saveData = ValueMap();
		saveData["MyLevel"] = Value(10);

		fileutil->writeValueMapToFile(saveData, saveFileFullpath);
		fileutil->writeValueVectorToFile(myItems, saveFileFullpath);
	});
	addChild(saveData);

	Button* loadData = Button::create("bin.png");
	loadData->setPosition(Vec2(400, 320));
	loadData->setTitleText("loadData ValuMap");
	loadData->setTitleColor(Color3B(1, 0, 0));
	loadData->addClickEventListener([](Ref* rf)->void{
		auto fileutil = FileUtils::getInstance();
		std::string path = fileutil->getWritablePath();
		CCLOG("Writable path : %s", path.c_str());

		std::string saveFileFullpath = path + "myData.xml";
		CCLOG("Save File Full- Path: %s", saveFileFullpath);

		// == Prepare Save file 
		ValueMap dict = fileutil->getValueMapFromFile(saveFileFullpath);

		int myLevel = dict["MyLevel"].asInt();
		ValueVector myItems = fileutil->getValueVectorFromFile(saveFileFullpath);
		CCLOG("My Level : %d",myLevel);

		CCLOG("Items : %s",myItems);
	});
	addChild(loadData);
    return true;
}
