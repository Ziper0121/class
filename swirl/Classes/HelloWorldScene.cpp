#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#define PI 3.141592

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	DrawNode* swirl = DrawNode::create();

	int k = 0;
	Vec2 two(0,0);
	Vec2 origin(60, 0);
	int R = 60;
	for (int i = 0; i < 5; i++){
		for (; k < 100; k++){
			two.x = R * cos((double)k * PI / 180);
			two.y = R * sin((double)k * PI / 180);
			if ((int)two.y == 0 && i > 0) origin = two; break;
			swirl->drawLine(origin, two, Color4F(1, 1, 1, 1));
			origin = two;
		}
	}
	swirl->setPosition(400, 300);
	addChild(swirl);
	
	return true;
}
