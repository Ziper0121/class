#ifndef __READ_ARTICLE_H__
#define __READ_ARTICLE_H__

#include "cocos2d.h"

class ReadArticle : public cocos2d::Layer
{
public:
	static cocos2d::Scene* ReadScene(int namber);

	virtual bool init();

	CREATE_FUNC(ReadArticle);

	void Readmain(int);
	void BackButton(cocos2d::Vec2,char*);

	void BackLisener(cocos2d::Ref* rf);

	void ArticleLabel(int);
};

#endif