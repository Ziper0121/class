#include "HelloWorldScene.h"
#include "Article_ListScene.h"

#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include <cstdio>
#include <fstream>
#include <iostream>
#include <ostream>
#include <istream>

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
	}
	HelloWorld::firstFileCreate("User.txt");
	HelloWorld::firstFileCreate("Title.txt");
	HelloWorld::firstFileCreate("Contents.txt");
	HelloWorld::EditBox(Vec2(300, 300));
	HelloWorld::button(Vec2(200, 200), "Login");
	HelloWorld::button(Vec2(300, 200), "SingUp");
	return true;
}

//에디트 박스 함수
void HelloWorld::EditBox(Vec2 Vc){
	Edet = EditBox::create(Size(300, 50), "bin.png");
	Edet->setPosition(Vc);
	Edet->setFontColor(Color3B::BLACK);
	Edet->setFontSize(20);
	addChild(Edet);
}

//Ui를 만들고 그 값이 같을 경우 스케줄도 생성
void HelloWorld::button(Vec2 vec, char* text){
	Button* Button = Button::create("bin.png");
	Button->setTitleText(text);
	Button->setTitleColor(Color3B(1, 1, 0));
	Button->setPosition(vec);
	addChild(Button);
	//여거 bind에 값을 집어넣는 법을 여쭤보자
	if (!strcmp(text, "Login")){
		Button->addClickEventListener(std::bind(&HelloWorld::LoginButton, this, std::placeholders::_1));
	}
	if (!strcmp(text, "SingUp"))
		Button->addClickEventListener(std::bind(&HelloWorld::SingUpButton, this, std::placeholders::_1));
}

//로그인 리스너
void HelloWorld::LoginButton(Ref* rf){
	scheduleOnce(schedule_selector(HelloWorld::LoginScedule,this,std::placeholders::_1),0.1f);
}

//회원가입 리스너
void HelloWorld::SingUpButton(Ref* rf){
	FILE* SingUp = fopen("User.txt", "a");
	fprintf(SingUp, Edet->getText());
	fprintf(SingUp, "\n");
	fclose(SingUp);
}

//로그인 스케줄
void HelloWorld::LoginScedule(float dt){
	std::ifstream ifFile = std::ifstream("User.txt", std::ios::in);
	ifFile.seekg(0, std::ios::beg);
	for (int i = 0; !ifFile.eof(); i++){
		char User[100];
		std::string string;
		ifFile.getline(User, 100);
		if (strcmp(User, Edet->getText()) == 0)
			Director::getInstance()->runWithScene(Article::ArticleScene());
	}
	ifFile.close();
}
void HelloWorld::firstFileCreate(char* filename){
	FILE* firstCreate = fopen(filename, "a");
	fclose(firstCreate);
}