#ifndef __ARTICLE_LISTSCENE_H__
#define __ARTICLE_LISTSCENE_H__

#include "cocos2d.h"

class Article : public cocos2d::Layer
{
public:
	static cocos2d::Scene* ArticleScene();

	virtual bool init();

	CREATE_FUNC(Article);

	void buttoncreate(char* ,cocos2d::Vec2 ,int = 100);

	std::vector<std::string> ArticleList;

	void BackButton(cocos2d::Ref* rf);
	void WritingButton(cocos2d::Ref* rf);
	void ReadArticle(cocos2d::Ref* rf);
};
#endif