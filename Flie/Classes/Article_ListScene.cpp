#include "HelloWorldScene.h"
#include "Article_ListScene.h"
#include "writingScene.h"
#include "ReadArticleScene.h"
#include "cocostudio/CocoStudio.h"
#include <cstdio>
#include <fstream>
#include <iostream>
#include <ostream>
#include <istream>

using namespace cocos2d;
using namespace cocos2d::ui;

Scene* Article::ArticleScene(){

	auto scene = Scene::create();

	auto article = Article::create();

	// add layer as a child to scene
	scene->addChild(article);

	// return the scene
	return scene;
}

bool Article::init()
{
	if (!Layer::init())
	{
		return false;
	}
	std::ifstream Title = std::ifstream("Title.txt", std::ios::in);
	for (int i = 0;!Title.eof();i++){
		char instead[100];
		Title.getline(instead,100);
		ArticleList.push_back(instead);
		Article::buttoncreate((char*)ArticleList[i].c_str(), Vec2(40, 600 - i * 80), i+1);
	}
	Title.close();
	Article::buttoncreate("Back", Vec2(40, 40));
	Article::buttoncreate("Writing", Vec2(920, 40));
	return true;
}

//버튼 생성
void Article::buttoncreate(char* name, Vec2 po,int tag){
	Button* create = Button::create("bin.png");
	create->setPosition(po);
	create->setTitleText(name);
	create->setTitleColor(Color3B::BLACK);
	CCLOG("%d", tag);
	if (tag !=100)
	create->setTag(tag);

	addChild(create);
	if (name == "Back")
	create->addClickEventListener(std::bind(&Article::BackButton, this, std::placeholders::_1));
	else if (name == "Writing")
		create->addClickEventListener(std::bind(&Article::WritingButton, this, std::placeholders::_1));
	else{
		create->addClickEventListener(std::bind(&Article::ReadArticle, this, std::placeholders::_1));
	}
}

//전 화면으로 이동
void Article::BackButton(cocos2d::Ref* rf){
	Director::getInstance()->runWithScene(HelloWorld::createScene());
}

//글 쓰기로 이동
void Article::WritingButton(cocos2d::Ref* rf){
	Director::getInstance()->runWithScene(Writing::WritingScene());
}
//글읽기 세인 비담 리 세인 니아 클로에 에반
void Article::ReadArticle(cocos2d::Ref* rf){
	Button* Tag = (Button*)rf;
	CCLOG("%d \n \n", Tag->getTag());
	Director::getInstance()->runWithScene(ReadArticle::ReadScene(Tag->getTag()));
}