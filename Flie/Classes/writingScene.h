#ifndef __WRITING_SCENE_H__
#define __WRITING_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class Writing : public cocos2d::Layer
{
public :
	static cocos2d::Scene* WritingScene();

	virtual bool init();

	CREATE_FUNC(Writing);

	//제목
	cocos2d::ui::EditBox* title;
	//글 내용
	cocos2d::ui::EditBox* Contents;

	//파일 좀더 간단하게
	void FileCreate(const char*, const char*);

	//ui 만들기
	cocos2d::ui::EditBox* EditCreate(cocos2d::Size, cocos2d::Vec2,cocos2d::ui::EditBox*);
	void ButtonCreate(cocos2d::Vec2, char*);

	void ButtonSave(cocos2d::Ref* rf);
	void ButtonBack(cocos2d::Ref* rf);
};

#endif