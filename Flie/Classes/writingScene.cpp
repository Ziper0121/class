#include "Article_ListScene.h"
#include "cocostudio/CocoStudio.h"
#include "writingScene.h"

using namespace cocos2d;
using namespace cocos2d::ui;

Scene* Writing::WritingScene(){

	Scene* newScene = Scene::create();

	Writing* writing = Writing::create();

	newScene->addChild(writing);

	return newScene;
}

bool Writing::init(){
	if (!Layer::init()){
		return false;
	}
	//EditBox 
	title = Writing::EditCreate(Size(700, 60), Vec2(480, 510), title);
	Contents = Writing::EditCreate(Size(700, 300), Vec2(480, 220), Contents);

	//Button
	Writing::ButtonCreate(Vec2(20, 20), "Save");
	Writing::ButtonCreate(Vec2(920, 20), "Back");
	return true;
}

//EditBox를 만들어 주는 함수
EditBox* Writing::EditCreate(Size si,Vec2 Vec, EditBox* Eb){
	Eb = EditBox::create(si, "bin.png");
	Eb->setPosition(Vec);
	Eb->setFontColor(Color3B::BLACK);
	addChild(Eb);
	return Eb;
}
//Button을 만드는 함수
void Writing::ButtonCreate(Vec2 Vec, char* ch){
	Button* bu = Button::create("bin.png");
	bu->setPosition(Vec);
	bu->setTitleText(ch);
	bu->setTitleColor(Color3B::BLACK);
	addChild(bu);


	if ("Save" == ch)
		bu->addClickEventListener(std::bind(&Writing::ButtonSave, this, std::placeholders::_1));

	if ("Back" == ch)
		bu->addClickEventListener(std::bind(&Writing::ButtonBack, this, std::placeholders::_1));
}

//뒤로 이동하는 uiListener
void Writing::ButtonBack(Ref* rf){
	Director::getInstance()->runWithScene(Article::ArticleScene());
}
//피일에 넣고 이동하는 uiListener
void Writing::ButtonSave(Ref* rf){
	Writing::FileCreate("Title.txt", title->getText());
	Writing::FileCreate("Contents.txt", Contents->getText());
	Director::getInstance()->runWithScene(Article::ArticleScene());
}

//파일 만들어주는 함수
void Writing::FileCreate(const char* txt,const char* Text){
	FILE* Save = fopen(txt, "a");
	fprintf(Save, Text);
	fprintf(Save, "\n");
	fclose(Save);
}