#include "ReadArticleScene.h"
#include "Article_ListScene.h"
#include "ui/CocosGUI.h"
#include <cstdio>
#include <fstream>
#include <iostream>
#include <ostream>
#include <istream>

using namespace cocos2d;
using namespace cocos2d::ui;

Scene* ReadArticle::ReadScene(int namber){

	Scene* Scene = Scene::create();
	
	ReadArticle* Read = ReadArticle::create();
	Scene->addChild(Read);
	Read->ReadArticle::Readmain(namber);

	return Scene;
}
void ReadArticle::Readmain(int TagNumber){

	ReadArticle::ArticleLabel(TagNumber);
	ReadArticle::BackButton(Vec2(900, 40), "Back");
}
bool ReadArticle::init(){
	if (!Layer::init())
	{
		return false;
	}
	return true;
}

void ReadArticle::BackButton(cocos2d::Vec2 V2, char* Text){
	Button* button = Button::create("bin.png");
	button->setTitleText(Text);
	button->setPosition(V2);
	button->setTitleColor(Color3B::BLACK);
	addChild(button);

	button->addClickEventListener(std::bind(&ReadArticle::BackLisener, this, std::placeholders::_1));
}

void ReadArticle::BackLisener(cocos2d::Ref* rf){
	Director::getInstance()->runWithScene(Article::ArticleScene());
}

void ReadArticle::ArticleLabel(int TagNumber){
	std::ifstream FTitle = std::ifstream("Title.txt", std::ios::in);
	std::ifstream FContents = std::ifstream("Contents.txt", std::ios::in);
	char Title[100];
	char Contents[100];
	for (int i = 0; i < TagNumber; i++){
		FTitle.getline(Title, 100);
		FContents.getline(Contents, 100);
	}
	FTitle.close();
	FContents.close();
	Label* ContentsLabel = Label::create(Contents, "04B_21__.txt",24);
	ContentsLabel->setPosition(Vec2(480,320));
	ContentsLabel->setTextColor(Color4B::WHITE);
	addChild(ContentsLabel);
	Label* TitleLabel = Label::create(Title, "04B_21__.txt", 24);
	TitleLabel->setPosition(Vec2(480, 600));
	TitleLabel->setTextColor(Color4B::WHITE);
	addChild(TitleLabel);
}