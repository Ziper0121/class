#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "ui/CocosGUI.h"
#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);

	cocos2d::ui::EditBox* Edet;

	//유아이들을 모두 그려놓는 함수
	void EditBox(cocos2d::Vec2);

	void button(cocos2d::Vec2, char*);

	//로그인이 가능 여부를 확인하는 스케줄
	void LoginScedule(float dt);
	bool Loginpoossible = false;

	//버튼이 클릭되면 스케줄 실행
	void LoginButton(Ref*);
	bool Click = false;

	void SingUpButton(cocos2d::Ref* rf);

	//먼저 필요한 파일을 모두 생성
	void firstFileCreate(char*);

};

#endif // __HELLOWORLD_SCENE_H__
