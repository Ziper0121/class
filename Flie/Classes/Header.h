#ifndef __Article_List_SCENE_H__
#define __Article_List_SCENE_H__

#include "ui/CocosGUI.h"
#include "cocos2d.h"

class Article_List : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(Article_List);



};

#endif