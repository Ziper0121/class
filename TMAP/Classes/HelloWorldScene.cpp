#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    auto scene = Scene::create();
    auto layer = HelloWorld::create();
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	map = TMXTiledMap::create("Map.tmx");
	addChild(map);

	Grounds = map->getLayer("Grounds");

	sprPanda = Sprite::create("Panda1.png");

	Vec2 posStart = Grounds->getPositionAt(Vec2(3, 12)) + Vec2(16, 16);
	sprPanda->setPosition(posStart);
	addChild(sprPanda);
	for (int i = 0; i < 268; i++) Keycode[i] = false;

	EventListenerKeyboard* Keyboard = EventListenerKeyboard::create();
	Keyboard->onKeyPressed = std::bind(&HelloWorld::KeyDown, this, std::placeholders::_1, std::placeholders::_2);
	Keyboard->onKeyReleased = std::bind(&HelloWorld::Keyup, this, std::placeholders::_1, std::placeholders::_2);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(Keyboard, this);

	schedule(schedule_selector(HelloWorld::moveLogic));
    return true;
}

void HelloWorld::KeyDown(EventKeyboard::KeyCode code, Event* e){
	Keycode[static_cast<int>(code)] = true;
}
void HelloWorld::Keyup(EventKeyboard::KeyCode  code, Event* e){
	Keycode[static_cast<int>(code)] = false;
}
void HelloWorld::moveLogic(float dt){
	Vec2 pos = sprPanda->getPosition();

	float offset = 100.0f* dt;
	if (Keycode[static_cast<int>(EventKeyboard::KeyCode::KEY_RIGHT_ARROW)]) pos.x += offset;
	if (Keycode[static_cast<int>(EventKeyboard::KeyCode::KEY_LEFT_ARROW)]) pos.x -= offset;
	if (Keycode[static_cast<int>(EventKeyboard::KeyCode::KEY_UP_ARROW)]) pos.y += offset;
	if (Keycode[static_cast<int>(EventKeyboard::KeyCode::KEY_DOWN_ARROW)]) pos.y -= offset;

	//이동할 지점 판다의 Tile상의 위치 구하기
	Vec2 coord = pos - Vec2(16, 16);
	float tileWidth = map->getTileSize().width;
	float tileheight = map->getTileSize().height;
	coord.x /= tileWidth;
	coord.y /= tileheight;
	
	//내림값 구하기 [올림값 : ceil(...)]
	coord.x = floor(coord.x);
	coord.y = floor(coord.y);
	CCLOG("floor : %f", coord.y);

	//coord.y의 좌표를 역으로 바꿔줌.
	coord.y = (map->getMapSize().height - 1) - coord.y;
	CCLOG("mapSize.hright - 1 : %f", coord.y);
	
	sprPanda->setPosition(pos);
}