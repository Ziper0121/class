#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
using namespace cocos2d;

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(HelloWorld);

	Sprite* sprPanda;
	TMXTiledMap* map;
	TMXLayer* Grounds;
	bool Keycode[268];

	void KeyDown(EventKeyboard::KeyCode, Event*);
	void Keyup(EventKeyboard::KeyCode, Event*);
	
	void moveLogic(float);
};

#endif // __HELLOWORLD_SCENE_H__
