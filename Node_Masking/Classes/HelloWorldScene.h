#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	bool touchBegan(cocos2d::Touch* Tc, cocos2d::Event* Et);
	void touchMove(cocos2d::Touch* Tc, cocos2d::Event* Et);
	void touchEned(cocos2d::Touch* Tc, cocos2d::Event* Et);

	cocos2d::ClippingNode* cliping;
	cocos2d::Node* stencil;
};

#endif // __HELLOWORLD_SCENE_H__
