#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Sprite* sprite1 = Sprite::create("background.jpg");
	sprite1->setPosition(480, 320);
	this->addChild(sprite1);

	cliping = ClippingNode::create();
	stencil = Sprite::create();
	cliping->setStencil(stencil);

	Sprite* wood = Sprite::create("dson.jpg");
	wood->setPosition(480, 320);
	cliping->addChild(wood);
	cliping->setInverted(true);
	cliping->setAlphaThreshold(0.1f);

	addChild(cliping);

	auto ontouch = EventListenerTouchOneByOne::create();
	ontouch->onTouchBegan = std::bind(&HelloWorld::touchBegan, this, std::placeholders::_1, std::placeholders::_2);
	ontouch->onTouchMoved = std::bind(&HelloWorld::touchMove, this, std::placeholders::_1, std::placeholders::_2);
	ontouch->onTouchEnded = std::bind(&HelloWorld::touchEned, this, std::placeholders::_1, std::placeholders::_2);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(ontouch, this);

    return true;
}

bool HelloWorld::touchBegan(Touch* Tc, Event* Et){
	Sprite* sprite2 = Sprite::create("masker.png");
	sprite2->setPosition(Tc->getLocation());
	stencil->addChild(sprite2);

	return true;
}
void HelloWorld::touchMove(cocos2d::Touch* Tc, cocos2d::Event* Et){
	Sprite* sprite3 = Sprite::create("masker.png");
	sprite3->setPosition(Tc->getLocation());
	stencil->addChild(sprite3);
}
void HelloWorld::touchEned(cocos2d::Touch* Tc, cocos2d::Event* Et){
	Sprite* sprite4 = Sprite::create("masker.png");
	sprite4->setPosition(Tc->getLocation());
	stencil->addChild(sprite4);
}