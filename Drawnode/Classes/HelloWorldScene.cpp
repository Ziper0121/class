#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#define PI 3.141592

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	//깃 이그노어에서의 **/는 모든을 뜻한다.


	DrawNode* drawnode = DrawNode::create();

	Vec2 fan[28];
	Vec2 Handle[28];
	Vec2 Zero(0,0);
	float Length = 0.0f;
	fan[0] = Zero;
	Handle[0] = Zero;
	for (int i = 1; i < 28; i++){
		if(i%2 == 0) Length = 90.0f;
		else Length = 100.0f;
		//PI / 각도   각도에 들어가는 값은 각도의 쎄기이다.
		fan[i].x = Length * cos((double)(i+1)*12 * PI / 360);
		fan[i].y = Length * sin((double)(i+1)*12 * PI / 360);
		Handle[i].x = 30 * cos((double)(i+31) * 12 * PI / 360);
		Handle[i].y = 30 * sin((double)(i+31) * 12 * PI / 360);
	}
	//number Of Points는 마지막에 처음 포인터와 연결이 된다.
	drawnode->drawSolidPoly(fan, 28, Color4F(1, 0, 0, 1));
	drawnode->drawSolidPoly(Handle, 28, Color4F(1, 1, 1, 1));
	drawnode->setPosition(400, 300);
	addChild(drawnode);
    return true;
}
