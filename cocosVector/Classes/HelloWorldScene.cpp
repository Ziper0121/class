#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	Sprite* sprite;
	Sprite* sprite1;

	for (int i = 0; i < 10; i++){
		sprite = Sprite::create("bird.png");
		sprite->setPosition(100 + i * 80, 200);
		stdVector.push_back(sprite);
	}

	for (int i = 0; i < 10; i++){
		sprite1 = Sprite::create("bird.png");
		sprite1->setPosition(100 + i * 80, 400);
		ccVector.pushBack(sprite1);
	}

	Button* btnStdVector = Button::create("btn_up.png");
	btnStdVector->setPosition(Vec2(800, 200));
	btnStdVector->setTitleText("StdVector AddChild");
	btnStdVector->setTitleColor(Color3B::BLACK);
	addChild(btnStdVector);

	btnStdVector->addClickEventListener(std::bind(&HelloWorld::onStdButtonClick, this, std::placeholders::_1));

	Button* btnCCVector = Button::create("btn_up.png");
	btnCCVector->setPosition(Vec2(800, 300));
	btnCCVector->setTitleText("CCVector AddChild");
	btnCCVector->setTitleColor(Color3B::BLACK);
	addChild(btnCCVector);

	btnCCVector->addClickEventListener(std::bind(&HelloWorld::onCCButtonClick, this, std::placeholders::_1));

    return true;
}

void HelloWorld::onStdButtonClick(Ref* rf){
	for (auto i = stdVector.begin(); i < stdVector.end(); i++) addChild((*i));
}
void HelloWorld::onCCButtonClick(Ref* rf){
	for (auto i = ccVector.begin(); i < ccVector.end(); i++) addChild((*i));
}