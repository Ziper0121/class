#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#define GL_VARIOUS 15
#define BTN_WIDTH 200
#define BTN_HEIGHT 50
#define LIST_WIDTH (BTN_WIDTH+20)
#define LIST_HEIGHT 600
char* blendsTitle[GL_VARIOUS] = {
	"GL_ZERO",
	"GL_ONE",
	"GL_SRC_COLOR",
	"GL_DST_COLOR",
	"GL_SRC_ALPHA",
	"GL_DST_ALPHA",
	"GL_SRC_ALPHA_SATURATE",
	"GL_CONSTANT_COLOR",
	"GL_CONSTANT_ALPHA",
	"GL_ONE_MINUS_SRC_COLOR",
	"GL_ONE_MINUS_DST_COLOR",
	"GL_ONE_MINUS_SRC_ALPHA",
	"GL_ONE_MINUS_DST_ALPHA",
	"GL_ONE_MINUS_CONSTANT_COLOR",
	"GL_ONE_MINUS_CONSTANT_ALPHA"
};
GLenum blends[GL_VARIOUS] = {
	GL_ZERO,
	GL_ONE,
	GL_SRC_COLOR,
	GL_DST_COLOR,
	GL_SRC_ALPHA,
	GL_DST_ALPHA,
	GL_SRC_ALPHA_SATURATE,
	GL_CONSTANT_COLOR,
	GL_CONSTANT_ALPHA,
	GL_ONE_MINUS_SRC_COLOR,
	GL_ONE_MINUS_DST_COLOR,
	GL_ONE_MINUS_SRC_ALPHA,
	GL_ONE_MINUS_DST_ALPHA,
	GL_ONE_MINUS_CONSTANT_COLOR,
	GL_ONE_MINUS_CONSTANT_ALPHA
};

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	Sprite* mosaicBg = Sprite::create("mosaic.jpg");
	mosaicBg->setPosition(480, 320);
	addChild(mosaicBg);

	Sprite* bg = Sprite::create("background.jpg");
	bg->setPosition(300, 320);
	addChild(bg);

	Sprite* blendTargetNode = Sprite::create("rgb.jpg");
	blendTargetNode->setPosition(300, 320);
	addChild(blendTargetNode);

	Label* label = Label::createWithSystemFont("Src (Foreground)", "04B_21__.ttf", 20.0f);
	label->setPosition(960 - (LIST_WIDTH * 2) + (LIST_WIDTH / 2), LIST_HEIGHT + 20.0f);
	addChild(label);
	ListView* listSource = ListView::create();
	listSource->setContentSize(Size(LIST_WIDTH, LIST_HEIGHT));
	listSource->setPosition(Vec2(960 - (LIST_WIDTH * 2), 0));
	addChild(listSource);
	
	label = Label::createWithSystemFont("Dest (Background)", "04B_21__.ttf", 20.0f);
	label->setPosition(960 - (LIST_WIDTH) + (LIST_WIDTH / 2), LIST_HEIGHT + 20.0f);
	addChild(label);
	ListView* listDest = ListView::create();
	listDest->setContentSize(Size(LIST_WIDTH, LIST_HEIGHT));
	listDest->setPosition(Vec2(960 - LIST_WIDTH, 0));
	addChild(listDest);

	glBlendColor(1.0f, 0.0f, 0.0f, 1.0f);
	for (int i = 0; i < GL_VARIOUS; i++){
		createButton(listSource, blendsTitle[i], [=](Ref* ref)->void {
			BlendFunc func;
			func = blendTargetNode->getBlendFunc();
			func.src = blends[i];
			blendTargetNode->setBlendFunc(func);
		});
		createButton(listDest, blendsTitle[i], [=](Ref* ref)->void {
			BlendFunc func;
			func = blendTargetNode->getBlendFunc();
			func.dst = blends[i];
			blendTargetNode->setBlendFunc(func);
		});
	}
    return true;
}
Button* HelloWorld::createButton(Node* parent, std::string title, const Button::ccWidgetClickCallback& click){
	
	Button* button = Button::create("Button.png");
	button->setScale9Enabled(true);
	button->setContentSize(Size(BTN_WIDTH, BTN_HEIGHT));
	button->setTitleText(title);
	button->addClickEventListener(click);
	parent->addChild(button);
	return button;
}