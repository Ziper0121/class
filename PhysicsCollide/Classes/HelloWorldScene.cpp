#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
#define TAG_PLATFORM 1
#define TAG_PLAYER 2

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
    auto layer = HelloWorld::create();
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    return true;
}
void HelloWorld::onEnter(){
	Layer::onEnter();
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	
	Node* stair2 = Node::create();
	PhysicsBody* stairBody = PhysicsBody::createBox(Size(200,30));
	stairBody->getShapes().at(0)->setTag(TAG_PLATFORM);
	stairBody->setContactTestBitmask(0x00000001);
	stairBody->setDynamic(false);
	stair2->setPhysicsBody(stairBody);
	stair2->setPosition(480, 220);
	addChild(stair2);

	Node* player = Node::create();
	PhysicsBody* playerBody = PhysicsBody::createBox(Size(40, 40));
	playerBody->getShapes().at(0)->setTag(TAG_PLAYER);
	playerBody->setContactTestBitmask(0x00000001);
	playerBody->setMass(10.0f);
	player->setPhysicsBody(playerBody);
	player->setPosition(480, 120);
	addChild(player);

	playerBody->applyImpulse(Vec2(0, 2000));

	auto collide = EventListenerPhysicsContact::create();
	collide->onContactBegin = std::bind(&HelloWorld::onPhiscsContactBegin, this, std::placeholders::_1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(collide, this);
}
bool HelloWorld::onPhiscsContactBegin(PhysicsContact& contact){
	int tagA = contact.getShapeA()->getTag();
	int tagB = contact.getShapeB()->getTag();

	// B를 기준으로 A의 속도 방향

	const PhysicsContactData* data = contact.getContactData();
	if (tagA == TAG_PLAYER && tagB == TAG_PLATFORM){
		CCLOG("A : PLAYER, B : PLATFORM");
		CCLOG("normal : (%f, %f)", data->normal.x, data->normal.y);
		// A가 플레이어일 경우.A의 아래가 B(플랫폼)와 접촉방향일 경우 true 리턴,
		//(True 리턴시 접촉 감지 활성 | false 리턴시 접촉 루틴 무시)
		return data->normal.y < 0;
	}

	if (tagA == TAG_PLATFORM && tagB == TAG_PLAYER){
		CCLOG("A : PLATFORM, B : PLAYER");
		CCLOG("normal : (%f, %f)", data->normal.x, data->normal.y);
		// A가 플레이어일 경우.A의 아래가 B(플랫폼)와 접촉방향일 경우 true 리턴,
		//(True 리턴시 접촉 감지 활성 | false 리턴시 접촉 루틴 무시)
		return data->normal.y > 0;
	}
	return true;
}
bool HelloWorld::onPhiscsContactPreSolve(PhysicsContact& contact, PhysicsContactPreSolve& solve){
	CCLOG("onPhiscsContactPreSolve");
	return true;
}
void HelloWorld::onPhiscsContactPostSolve(PhysicsContact& contact,const PhysicsContactPostSolve& solve){
	CCLOG("onPhiscsContactPostSolve");
}
void HelloWorld::onPhiscsContactSeparate(PhysicsContact& contact){
	CCLOG("onPhiscsContactSeparate");
}