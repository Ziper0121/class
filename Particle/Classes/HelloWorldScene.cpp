#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	char* btnNames[12] = {
		"Particle Full",
		"Fire",
		"FireWorks",
		"Sun",
		"Galaxy",
		"Flower",
		"Meteor",
		"Spiral",
		"smoke",
		"Snow",
		"Rain",
		"Explosion",
	};

	TextureCache* tc = Director::getInstance()->getTextureCache();

	Texture2D* fill_circle = tc->addImage("Fill_circle.png");
	Texture2D* fill_hexagon = tc->addImage("Fill_Gexagon.png");
	Texture2D* fill_rectangle = tc->addImage("fill_rectangle.png");
	Texture2D* fill_star = tc->addImage("fill_star.png");
	Texture2D* grow_circle = tc->addImage("grow_circle.png");
	Texture2D* grow_snow = tc->addImage("grow_snow.png");
	Texture2D* grow_star = tc->addImage("grow_star.png");
	Texture2D* line = tc->addImage("line.png");
	Texture2D* line_circle = tc->addImage("line_circle.png");
	Texture2D* line_hexagon = tc->addImage("line_hexagon.png");
	Texture2D* line_rectangle = tc->addImage("line_rectangle.png");
	Texture2D* line_star = tc->addImage("line_star.png");
	Texture2D* smoke = tc->addImage("smoke.png");
	Texture2D* snow = tc->addImage("snow.png");

	for (int i = 0; i < 6; i++){
		Button* btn = Button::create("btn_up.png");
		btn->setPosition(Vec2(100 + i * 140, 200));
		btn->setTitleText(btnNames[i]);
		addChild(btn);
		btn->setTitleColor(Color3B::BLACK);
		btn->addClickEventListener(std::bind(&HelloWorld::btnClick, this, std::placeholders::_1));
		btn->setTag(i);
	}

	for (int i = 6; i < 12; i++){
		Button* btn = Button::create("btn_up.png");
		btn->setPosition(Vec2(100 + (i-6)* 140, 120));
		btn->setTitleText(btnNames[i]);
		addChild(btn);
		btn->setTitleColor(Color3B::BLACK);
		btn->addClickEventListener(std::bind(&HelloWorld::btnClick, this, std::placeholders::_1));
		btn->setTag(i);
	}

    return true;
}

void HelloWorld::btnClick(Ref* ref){
	Button* btn = (Button*)ref;
	if (system != nullptr) system->removeFromParent();
	switch (btn->getTag())
	{
		//여기 이름을 제대로 하자
	case 0:testParticleFull(); break;
	case 1:testParticleFire(); break;
	case 2:testParticleFireworks(); break;
	case 3:testParticlesun(); break;
	case 4:testParticleGalaxy(); break;
	case 5:testParticleFlower(); break;
	case 6:testParticleMeteor(); break;
	case 7:testParticleSpiral(); break;
	case 8:testParticleSmoke(); break;
	case 9:testParticlesnow(); break;
	case 10:testParticleRain(); break;
	case 11:testParticleExplosing(); break;

	}
}

void HelloWorld::testParticleFull(){
	Texture2D* texture = Director::getInstance()->getTextureCache()->addImage("line.png");

	system = ParticleFireworks::create();
	system->setTexture(texture);
	system->setPosition(480, 320);
	addChild(system);

	this->system = system;
}
void HelloWorld::addParticle(std::string textureName, ParticleSystem* system){
	system->setTexture(
		Director::getInstance()->getTextureCache()->getTextureForKey(textureName));
	system->setPosition(480, 320);
	addChild(system);

	this->system = system;
}

//Rin에서 오류
void HelloWorld::testParticleFire(){
	ParticleSystem* system = ParticleFire::create();
	addParticle("smoke.png", system);
}
void HelloWorld::testParticleFireworks(){
	ParticleSystem* system = ParticleFireworks::create();
	addParticle("fill_star.png", system);
}
void HelloWorld::testParticlesun(){
	ParticleSystem* system = ParticleSun::create();
	addParticle("grow_star.png", system);
}
void HelloWorld::testParticleGalaxy(){
	ParticleSystem* system = ParticleGalaxy::create();
	addParticle("line_hexagon.png", system);
}
void HelloWorld::testParticleFlower(){
	ParticleSystem* system = ParticleFlower::create();
	addParticle("fill_star.png", system);
}
void HelloWorld::testParticleMeteor(){
	ParticleSystem* system = ParticleMeteor::create();
	addParticle("grow_circle.png", system);
}
void HelloWorld::testParticleSpiral(){
	ParticleSystem* system = ParticleSpiral::create();
	addParticle("line.png", system);
}
void HelloWorld::testParticleSmoke(){
	ParticleSystem* system = ParticleSmoke::create();
	addParticle("smoke.png", system);
}
void HelloWorld::testParticlesnow(){
	ParticleSystem* system = ParticleSnow::create();
	addParticle("snow.png", system);
}
void HelloWorld::testParticleRain(){
	ParticleSystem* system = ParticleRain::create();
	addParticle("grow_circle.png", system);
}
void HelloWorld::testParticleExplosing(){
	ParticleSystem* system = ParticleExplosion::create();
	addParticle("grow_star.png", system);
}