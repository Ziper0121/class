#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "json/rapidjson.h"
#include "json/document.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	HelloWorld::parseAdvance();
    return true;
}

void HelloWorld::parseSimple(){
	char* json;
	json = "{\"name\":\"Tina\",\"age\":24}";
	rapidjson::Document d;
	d.Parse(json);

	bool parseError = d.HasParseError();
	// == 파싱 에러 처리
	if (parseError) CCLOG("parse error : %d", parseError);

	std::string name = d["name"].GetString();
	int age = d["age"].GetInt();

	CCLOG("name: %s", name.c_str());
	CCLOG("age: %d", age);

}
void HelloWorld::parseAdvance(){
	using namespace rapidjson;

	char* filename = "json.txt";
	ssize_t fileSize;

	//이것이 recose파일에 있는 정보를 가져오는 것이다.
	Data FileData = FileUtils::getInstance()->getDataFromFile(filename);
	unsigned char* bytes = FileData.getBytes();
	ssize_t byte_count = FileData.getSize();

	std::string json_string = std::string((char*)bytes, 0, byte_count);

	CCLOG("readed:%s", json_string.c_str());

	// UTF-8 String
	StringStream ss = StringStream(json_string.c_str());

	Document d;

	d.ParseStream<kArrayType>(ss);

	bool parseError = d.HasParseError();
	if (parseError){
		CCLOG("parseError:%d", d.HasParseError());
		ParseErrorCode error = d.GetParseError();
		CCLOG("parseErrorCode:%d", error);
		return;
	}

	for (auto it = d.Begin(); it != d.End(); it++){
		const char* name = (*it)["name"].GetString();
		const char* start = (*it)["start"].GetString();
		const char* end = (*it)["end"].GetString();

		CCLOG("name: %s,start:%s,end:%s", name, start, end);
	}
}