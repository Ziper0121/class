#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	Button* btn = Button::create("mario.png");
	btn->setTitleText("AutoRelease");
	btn->setTitleColor(Color3B::BLACK);

	btn->setPosition(Vec2(380, 200));
	btn->addClickEventListener(CC_CALLBACK_1(HelloWorld::clickAutoRelease, this));
	addChild(btn);
	
	sprite1 = Sprite::create("mario1.png");
	sprite1->setPosition(380, 400);
	addChild(sprite1);

	autoReleaseAction = ScaleBy::create(1.0f, 1.5f);
	//autoReleaseAction;
	//retainAction;
	//sprite2;
	
    return true;
}

void HelloWorld::clickAutoRelease(Ref* ref){
	sprite1->runAction(autoReleaseAction);
}