#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__
#include "ui/CocosGUI.h"
#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);


	/*파티클 생성*/   //setAutoRemoveOnFinish를 true로 해주어야 한다.

	ParticleSystem* Particle_to_make(std::string textureName);

	/*파티클 설정*/

	//시작 각도 설정
	void set_Angle(float angle, ParticleSystem* Particle);
	void set_AngleVar(float angleVar, ParticleSystem* Particle);

	//분사량
	void set_EmissingRate(float rate, ParticleSystem* Particle);

	//파티클 분촐의 모드를 설정
	void set_EmitterMode(ParticleSystem::Mode Mode, ParticleSystem* Particle);

	//파티클 분출 위치
	void set_PosVar(float RI_Lef, float Up_D ,ParticleSystem* Particle);

	/*파티클 입자 설정*/

	//시작 입자 크기 설정
	void set_StartSize(float size, ParticleSystem* Particle);
	void set_StartSizeVar(float sizeVar, ParticleSystem* Particle);

	//종료 입자 크기
	void set_EndSize(float size, ParticleSystem* Particle);
	void set_EndSizeVar(float sizeVar, ParticleSystem* Particle);

	//시작 입자 회전
	void set_StarSpin(float spin, ParticleSystem* Particle);
	void set_StarSpinVar(float spinVar, ParticleSystem* Particle);

	//종료 입자 회전
	void set_EndSpin(float spin, ParticleSystem* Particle);
	void set_EndSpinVar(float spinVar, ParticleSystem* Particle);

	//시작 입자 색깔
	void set_StarColor(float R, float G, float B, float A, ParticleSystem* Particle);
	void set_StarColorVar(float R, float G, float B, float A, ParticleSystem* Particle);

	//종료 입자 색깔
	void set_EndColor(float R, float G, float B, float A, ParticleSystem* Particle);
	void set_EndColorVar(float R, float G, float B, float A, ParticleSystem* Particle);

	/*파티클 모드 설정 */

	/* 모두 가능   Both*/

	//진행 시간 설정
	void set_Duration(float second, ParticleSystem* Particle);

	//파티클 유지 시간 설정
	void set_Life(float life, ParticleSystem* Particle);
	void set_LifeVar(float lifeVar, ParticleSystem* Particle);

	/*각도 분사   Gravity Move*/

	//파티클의 속도를 설정
	void set_speed(float speed, ParticleSystem* Particle);
	void set_speedVar(float speedVar, ParticleSystem* Particle);

	//파티클 중력을 설정
	void set_Gavity(Vec2 vec, ParticleSystem* Particle);

	//파티클 방사를 설정
	void set_RadicalAccel(float accel, ParticleSystem* Particle);
	void set_RadicalAccelVar(float accelVar, ParticleSystem* Particle);

	//파티클 회전 설정  음수는 반시계 양수는 시계반향 
	void set_TangenticalAccel(float accel,ParticleSystem* Particle);
	void set_TangenticalAccelVar(float accelVar, ParticleSystem* Particle);

	/*윈 분사   Radius Mode*/

	//시작 원 위치
	void set_StartRadius(float radius, ParticleSystem* Particle);
	void set_StartRadiusVar(float radiusVar, ParticleSystem* Particle);

	//종료 원 위치
	void set_EndRadius(float radius, ParticleSystem* Particle);
	void set_EndRadiusVar(float radiusVar, ParticleSystem* Particle);

	//초당 회전 수 설정
	void set_RotatePerSecond(float rot, ParticleSystem* Particle);
	void set_RotatePerSecondVar(float rotVar, ParticleSystem* Particle);

	/*파티클 모양 설정*/

	/*버튼 만들기*/
	cocos2d::ui::Button* makeDefaultButton(std::string Name);

	/*슬라이더 만들기*/
	cocos2d::ui::Slider* makeDefaultSlider(float min, float max, int steps);
};

#endif // __HELLOWORLD_SCENE_H__
