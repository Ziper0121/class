#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

    return true;
}

Slider* HelloWorld::makeDefaultSlider(float min, float max, int steps){
	Slider* slider = Slider::create();
	slider->loadBarTexture("slider_empty.png");
	slider->loadProgressBarTexture("slider_fill.png");
	slider->loadSlidBallTextureNormal("slider_track.png");
	slider->setScale9Enabled(true);
	slider->setMaxPercent((max - min)* steps);
	LinearLayoutParameter* parameter = LinearLayoutParameter::create();
	parameter->setMargin(Margin(5, 0, 5, 0));
	slider->setLayoutParameter(parameter);
	slider->setTag(steps);
	return slider;
}

/*버튼 만들기*/
cocos2d::ui::Button* makeDefaultButton(std::string Name){
	Button* button = Button::create("btn_up.png", "btn_down.png");
	button->setTitleText(Name);
	button->setTitleColor(Color3B(1,0,1));
	return button;
}

/*파티클 생성*/   //setAutoRemoveOnFinish를 true로 해주어야 한다.
ParticleSystem* Particle_to_make(std::string textureName){
	ParticleSystem* system = ParticleSystem::create(textureName);
	return system;
}

/*파티클 설정*/

//시작 각도 설정
void set_Angle(float angle, ParticleSystem* Particle){
	Particle->setAngle(angle);
}
void set_AngleVar(float angleVar, ParticleSystem* Particle){
	Particle->setAngleVar(angleVar);
}

//분사량
void set_EmissionRate(float rate, ParticleSystem* Particle){
	Particle->setEmissionRate(rate);
}

//파티클 분촐의 모드를 설정
void set_EmitterMode(ParticleSystem::Mode Mode, ParticleSystem* Particle){
	Particle->setEmitterMode(Mode);
}

//파티클 분출 위치
void set_PosVar(float RI_Lef, float Up_D, ParticleSystem* Particle){
	Particle->setPosVar(Vec2(RI_Lef,Up_D));
}

/*파티클 입자 설정*/

//시작 입자 크기 설정
void set_StartSize(float size, ParticleSystem* Particle){
	Particle->setStartSize(size);
}
void set_StartSizeVar(float sizeVar, ParticleSystem* Particle){
	Particle->setStartSizeVar(sizeVar);
}

//종료 입자 크기
void set_EndSize(float size, ParticleSystem* Particle){
	Particle->setEndSize(size);
}
void set_EndSizeVar(float sizeVar, ParticleSystem* Particle){
	Particle->setEndSizeVar(sizeVar);
}

//시작 입자 회전
void set_StarSpin(float sipe, ParticleSystem* Particle){
	Particle->setStartSpin(sipe);
}
void set_StarSpinVar(float spinVar, ParticleSystem* Particle){
	Particle->setStartSpinVar(spinVar);
}

//종료 입자 회전
void set_EndSpin(float sipe, ParticleSystem* Particle){
	Particle->setEndSpin(sipe);
}
void set_EndSpinVar(float spinVar, ParticleSystem* Particle){
	Particle->setEndSpinVar(spinVar);
}

//시작 입자 색깔
void set_StarColor(float R, float G, float B, float A, ParticleSystem* Particle){
	Particle->setStartColor(Color4F(R, G, B, A));
}
void set_StarColorVar(float R, float G, float B, float A, ParticleSystem* Particle){
	Particle->setStartColorVar(Color4F(R, G, B, A));
}

//종료 입자 색깔
void set_EndColor(float R, float G, float B, float A, ParticleSystem* Particle){
	Particle->setEndColor(Color4F(R, G, B, A));
}
void set_EndColorVar(float R, float G, float B, float A, ParticleSystem* Particle){
	Particle->setEndColorVar(Color4F(R, G, B, A));
}

/*파티클 모드 설정 */

/* 모두 가능   Both*/

//진행 시간 설정
void set_Duration(float second, ParticleSystem* Particle){
	Particle->setDuration(second);
}

//파티클 유지 시간 설정
void set_Life(float life, ParticleSystem* Particle){
	Particle->setLife(life);
}
void set_LifeVar(float lifeVar, ParticleSystem* Particle){
	Particle->setLifeVar(lifeVar);
}

/*각도 분사   Gravity Move*/

//파티클의 속도를 설정
void set_speed(float speed, ParticleSystem* Particle){
	Particle->setSpeed(speed);
}
void set_speedVar(float speedVar, ParticleSystem* Particle){
	Particle->setSpeedVar(speedVar);
}

//파티클 중력을 설정
void set_Gavity(Vec2 vec, ParticleSystem* Particle){
	Particle->setGravity(vec);
}

//파티클 방사를 설정
void set_RadicalAccel(float accel, ParticleSystem* Particle){
	Particle->setRadialAccel(accel);
}
void set_RadicalAccelVar(float accelVar, ParticleSystem* Particle){
	Particle->setRadialAccelVar(accelVar);
}

//파티클 회전 설정  음수는 반시계 양수는 시계반향 
void set_TangenticalAccel(float accel, ParticleSystem* Particle){
	Particle->setTangentialAccel(accel);
}
void set_TangenticalAccelVar(float accelVar, ParticleSystem* Particle){
	Particle->setTangentialAccelVar(accelVar);
}

/*윈 분사   Radius Mode*/

//시작 원 위치
void set_StartRadius(float radius, ParticleSystem* Particle){
	Particle->setStartRadius(radius);
}
void set_StartRadiusVar(float radiusVar, ParticleSystem* Particle){
	Particle->setStartRadiusVar(radiusVar);
}

//종료 원 위치
void set_EndRadius(float radius, ParticleSystem* Particle){
	Particle->setEndRadius(radius);
}
void set_EndRadiusVar(float radiusVar, ParticleSystem* Particle){
	Particle->setEndRadiusVar(radiusVar);
}

//초당 회전 수 설정
void set_RotatePerSecond(float rot, ParticleSystem* Particle){
	Particle->setRotatePerSecond(rot);
}
void set_RotatePerSecondVar(float rotVar, ParticleSystem* Particle){
	Particle->setRotatePerSecondVar(rotVar);
}