#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

#define NODE_JUMP 1
#define NODE_ROTATE 2

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
    auto layer = HelloWorld::create();
    // add layer as a child to scene
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init()) return false;

	auto click = EventListenerMouse::create();

    return true;
}
void HelloWorld::onEnter(){
	Layer::onEnter();
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
}
Vec2 HelloWorld::makeEdgeBox(std::string name, Vec2 origin, Size size){
	PhysicsBody* body = PhysicsBody::createBox(size, PhysicsMaterial(0.0f, 0.0f, 1.0f));
	body->setPositionOffset(size / 2); // Offset(Anchor 1.0f 1.0f)
	body->setDynamic(false);
	Node* node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(origin);
	addChild(node);

	Label* label = Label::createWithSystemFont(name, "Arial.ttf", 12.0f);
	label->setPosition(origin + Vec2(size.width/2,size.height - 12.0f);
	addChild(label);
	return origin + size / 2;
}
Node* HelloWorld::createPhysicsBox(Vec2 pos, Size size){
	Node* node = Node::create();
	PhysicsBody* body = PhysicsBody::createBox(size, PhysicsMaterial(0.0f, 0.0f, 1.0f));
	body->setAngularDamping(0.2f);
	node->setPhysicsBody(body);
	node->setPosition(pos);
	addChild(node);
	return node;
}
void HelloWorld::onMouseDown(EventMouse* e){
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	//Vec2 loc = Vec2(e->getLocation().x,800-e->getLocation().y);
	Vec2 loc = Vec2(e->getCursorX, e->getCursorY());
	auto shape = world->getShape(loc);

	if (shape == nullptr)return;
	if (shape->getBody()->getNode()->getTag() == NODE_JUMP) shape->getBody()->applyImpulse(Vec2(0, 100), Vec2::ZERO);
	else if (shape->getBody()->getNode()->getTag() == NODE_ROTATE){
		if (e->getMouseButton() == 0){
			shape->getBody()->applyImpulse(Vec2(0, 50), Vec2(-20, 0));
			shape->getBody()->applyImpulse(Vec2(0, -50), Vec2(20, 0));
		}
		if (e->getMouseButton() == 1){
			shape->getBody()->applyImpulse(Vec2(0, 50), Vec2(-20, 0));
			shape->getBody()->applyImpulse(Vec2(0, -50), Vec2(20, 0));

		}
	}
}
void HelloWorld::makePhyiscsJonitFixed(Vec2, Vec2){

}
void HelloWorld::makePhysicsJointLimit(Vec2, Vec2){}
void HelloWorld::makePhysicsJointPin(Vec2, Vec2){}
void HelloWorld::makePhysicsJointDistance(Vec2, Vec2){}
void HelloWorld::makePhysicsJointSpring(Vec2, Vec2){}
void HelloWorld::makePhysicsJointGroove(Vec2, Vec2){}