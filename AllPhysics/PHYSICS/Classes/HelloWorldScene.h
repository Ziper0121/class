#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
using namespace cocos2d;

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
	void onEnter();

	Vec2 makeEdgeBox(std::string, Vec2, Size);
	Node* createPhysicsBox(Vec2, Size);
	void onMouseDown(EventMouse*);
	void makePhyiscsJonitFixed(Vec2, Vec2);
	void makePhysicsJointLimit(Vec2, Vec2);
	void makePhysicsJointPin(Vec2, Vec2);
	void makePhysicsJointDistance(Vec2, Vec2);
	void makePhysicsJointSpring(Vec2, Vec2);
	void makePhysicsJointGroove(Vec2, Vec2);
};

#endif // __HELLOWORLD_SCENE_H__
