#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

#define NODE_JUMP 1 
#define NODE_ROTATE 2

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	PhysicsWorld* world = scene->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);
    auto layer = HelloWorld::create();
	scene->addChild(layer);
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!Layer::init()) return false;

	auto click = EventListenerMouse::create();
	click->onMouseDown = std::bind(&HelloWorld::onMousDown, this, std::placeholders::_1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(click, this);

    return true;
}

void HelloWorld::onEnter(){
	Layer::onEnter();
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	world->setAutoStep(false);


	Size edgeSize = Size(200, 160);
	Vec2 offsetBase = edgeSize / 2;

	Vec2 boxCenter;
	Vec2 bodyOffset = Vec2(40, 0);
	std::string name[11] = {
		"PhysicsJointFixed",
		"PhysicsJointLimit",
		"PhysicsJointPin",
		"PhysicsJointDistance",
		"PhysicsJointSpring",
		"PhysicsJointGroove",
		"PhysicsJointRotarySpring",
		"PhysicsJointRotaryLimit",
		"PhysicsJointGear",
		"PhysicsJointMotor",
	};
	boxCenter = makeEdgeBox(name[0],
		offsetBase + Vec2(edgeSize.width * 0, edgeSize.height * 0), edgeSize);
	makePhysicsJointFixed(boxCenter - bodyOffset, boxCenter + bodyOffset);


}

Vec2 HelloWorld::makeEdgeBox(std::string name, Vec2 origin, Size size){
	PhysicsBody* body = PhysicsBody::createEdgeBox(size, PhysicsMaterial(0.0f, 0.0f, 1.0f));
	body->setPositionOffset(size / 2);  // Offset (Anchor 1.0f,1.0f)
	body->setDynamic(false);
	Node* node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(origin);
	addChild(node);

	Label* label = Label::createWithSystemFont(name, "04B_21__.TTF", 12.0f);
	label->setPosition(origin + Vec2(size.width / 2, size.height - 12.0f));
	addChild(label);

	return origin + size / 2;
}
Node* HelloWorld::createPhysicsBox(Size size, Vec2 pos){
	Node* node = Node::create();
	PhysicsBody* body = PhysicsBody::createBox(size, PhysicsMaterial(0.0f, 0.0f, 1.0f));
	body->setAngularDamping(0.2f);
	node->setPhysicsBody(body);
	node->setPosition(pos);
	addChild(node);
	return node;
}
void HelloWorld::onMousDown(EventMouse* e){
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	//Vec2 loc = Vec2(e->getLocation().x,800-e->getLocation().y);
	Vec2 loc = Vec2(e->getCursorX(), e->getCursorY());
	auto shape = world->getShape(loc);
	if (shape == nullptr)return;

	if (shape->getBody()->getNode()->getTag() == NODE_JUMP)
		shape->getBody()->applyImpulse(Vec2(0, 100), Vec2::ZERO);
	else if (shape->getBody()->getNode()->getTag() == NODE_ROTATE){
		if (e->getMouseButton() == 0){
			shape->getBody()->applyImpulse(Vec2(0, 50), Vec2(-20, 0));
			shape->getBody()->applyImpulse(Vec2(0, -50), Vec2(20, 0));
		}
		if (e->getMouseButton() == 1){
			shape->getBody()->applyImpulse(Vec2(0, -50), Vec2(-20, 0));
			shape->getBody()->applyImpulse(Vec2(0, 50), Vec2(20, 0));
		}
	}

}

void HelloWorld::makePhysicsJointFixed(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(40,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,40), (a + b) / 2);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointFixed::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		(a + b) / 2
	);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointLimit(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20, 20), a);
	Node* nodeB = createPhysicsBox(Size(20, 20), b);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointLimit::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE,
		40.0f,80.0f
		);
	getScene()->getPhysicsWorld()->addJoint(joint);

}
void HelloWorld::makePhysicsJointPin(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20, 20), a);
	Node* nodeB = createPhysicsBox(Size(20, 20), b);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointPin::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		(a+b)/2
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointDistance(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20, 20), a);
	Node* nodeB = createPhysicsBox(Size(20, 20), b);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointDistance::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointSpring(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20, 20), a);
	Node* nodeB = createPhysicsBox(Size(20, 20), b);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointSpring::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		Vec2(10, 0), Vec2(-10, 0),3.0f,0.5f
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointGroove(Vec2 a, Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20, 20), a);
	Node* nodeB = createPhysicsBox(Size(20, 20), b);
	nodeA->setTag(NODE_JUMP);
	nodeB->setTag(NODE_JUMP);

	PhysicsJoint* joint = PhysicsJointGroove::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		Vec2(40, 10), Vec2(40, -10),Vec2::ANCHOR_MIDDLE_LEFT
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}

void HelloWorld::makePhysicsJointRotarySpring(cocos2d::Vec2 a, cocos2d::Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,20), (a + b) / 2);
	nodeA->setTag(NODE_ROTATE);
	nodeB->setTag(NODE_ROTATE);
	nodeA->getPhysicsBody()->setGravityEnable(false);
	nodeB->getPhysicsBody()->setGravityEnable(false);

	PhysicsJoint* joint = PhysicsJointRotarySpring::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		10.0f,0.5f // stiffness, damping
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointRotaryLimit(cocos2d::Vec2 a, cocos2d::Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,20), (a + b) / 2);
	nodeA->setTag(NODE_ROTATE);
	nodeB->setTag(NODE_ROTATE);
	nodeA->getPhysicsBody()->setGravityEnable(false);
	nodeB->getPhysicsBody()->setGravityEnable(false);

	PhysicsJoint* joint = PhysicsJointRotaryLimit::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		10.0f, 20.0f // min(rotation), Max(rotation)
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointRatchet(cocos2d::Vec2 a, cocos2d::Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,20), (a + b) / 2);
	nodeA->setTag(NODE_ROTATE);
	nodeB->setTag(NODE_ROTATE);
	nodeA->getPhysicsBody()->setGravityEnable(false);
	nodeB->getPhysicsBody()->setGravityEnable(false);

	PhysicsJoint* joint = PhysicsJointRatchet::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		10.0f, 20.0f // min(rotation), Max(rotation)
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointGear(cocos2d::Vec2 a, cocos2d::Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,20), (a + b) / 2);
	nodeA->setTag(NODE_ROTATE);
	nodeB->setTag(NODE_ROTATE);
	nodeA->getPhysicsBody()->setGravityEnable(false);
	nodeB->getPhysicsBody()->setGravityEnable(false);

	PhysicsJoint* joint = PhysicsJointGear::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		0.0f, 0.2f // min(rotation), Max(rotation)
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}
void HelloWorld::makePhysicsJointMotor(cocos2d::Vec2 a, cocos2d::Vec2 b){
	Node* nodeA = createPhysicsBox(Size(20,20), (a + b) / 2);
	Node* nodeB = createPhysicsBox(Size(20,20), (a + b) / 2);
	nodeA->setTag(NODE_ROTATE);
	nodeB->setTag(NODE_ROTATE);
	nodeA->getPhysicsBody()->setGravityEnable(false);
	nodeB->getPhysicsBody()->setGravityEnable(false);

	PhysicsJoint* joint = PhysicsJointMotor::construct(
		nodeA->getPhysicsBody(), nodeB->getPhysicsBody(),
		0.6f// min(rotation), Max(rotation)
		);
	getScene()->getPhysicsWorld()->addJoint(joint);
}

void HelloWorld::PhysicsLogic(float dt){

}