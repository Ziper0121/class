#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include <string>
#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(HelloWorld);

	void onEnter();

	cocos2d::Vec2 makeEdgeBox(std::string , cocos2d::Vec2 , cocos2d::Size);
	cocos2d::Node* createPhysicsBox(cocos2d::Size, cocos2d::Vec2);

	void onMousDown(cocos2d::EventMouse* e);

	//Joint �Ÿ�
	void makePhysicsJointFixed(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointLimit(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointPin(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointDistance(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointSpring(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointGroove(cocos2d::Vec2, cocos2d::Vec2);


	//Joint ����
	void makePhysicsJointRotarySpring(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointRotaryLimit(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointRatchet(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointGear(cocos2d::Vec2, cocos2d::Vec2);
	void makePhysicsJointMotor(cocos2d::Vec2, cocos2d::Vec2);

	void PhysicsLogic(float);
};

#endif // __HELLOWORLD_SCENE_H__
