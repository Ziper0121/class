#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);
	layer->onEnter();

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    


    return true;
}
void HelloWorld::onEnter(){
	Layer::onEnter();
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	PhysicsBody* body;
	Node* node;

	body = PhysicsBody::createEdgeBox(Size(500, 400));
	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(480, 320);
	addChild(node);

	HelloWorld::experimental3();
	
}
void HelloWorld::experimental1(){
	PhysicsBody* body = PhysicsBody::createCircle(20);
	CCLOG("Body mass : %f", body->getMass());
	PhysicsShape* shape = PhysicsShapeBox::create(Size(40, 40));
	shape->setMass(30.0f);
	shape->setDensity(1.5f);

	body->addShape(shape);

	CCLOG("shape mass : %f",shape->getMass());
	CCLOG("shape density : %f",shape->getDensity());
	CCLOG("shape area : %f",shape->getArea());

	//Guess the body's mass
	CCLOG("Body mass : %f", body->getMass());

	Node* node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(480, 320);
	addChild(node);
}
void HelloWorld::experimental2(){
	PhysicsBody* body;
	Node* node;

	//Moment 0.1f
	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(400, 320);
	addChild(node);

	body->applyTorque(1.0f);
	body->setGravityEnable(false);


	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(560, 320);
	addChild(node);

	body->applyTorque(1.0f);
	body->setGravityEnable(false);
}
void HelloWorld::experimental3(){
	PhysicsBody* body;
	Node* node;

	//Moment 0.1f
	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(400, 320);
	addChild(node);

	body->setAngularVelocity(1.0f);
	body->setGravityEnable(false);


	//Moment 0.5f
	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.2f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(560, 320);
	addChild(node);

	body->setAngularVelocity(1.0f);
	body->setGravityEnable(false);

}
void HelloWorld::experimental4(){
	PhysicsBody* body;
	Node* node;

	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);
	body->setAngularDamping(0.5f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(400, 320);
	addChild(node);

	body->setAngularVelocity(10.0f);
	body->setGravityEnable(false);

	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);
	body->setAngularDamping(0.1f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(560, 320);
	addChild(node);

	body->setAngularVelocity(10.0f);
	body->setGravityEnable(false);
}
void HelloWorld::experimental5(){
	PhysicsBody* body;
	Node* node;

	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);
	body->setAngularDamping(0.5f);

	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(400, 320);
	addChild(node);

	body->applyTorque(1.0f);
	body->setGravityEnable(false);

	body = PhysicsBody::createBox(Size(40, 40));
	body->setMoment(0.1f);
	body->setAngularDamping(1.0f);
	
	node = Node::create();
	node->setPhysicsBody(body);
	node->setPosition(560, 320);
	addChild(node);

	body->applyTorque(1.0f);
	body->setGravityEnable(false);
}
