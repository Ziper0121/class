#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(HelloWorld);

	void onEnter();
	void makeTerrain();

	Node* circleNode1;
	Node* circleNode2;
	Node* circleNode3;
	Node* circleNode4;

	void makeCircle();

	void experimental1();
	void experimental2();
	void experimental3();
	void experimental4();
};

#endif // __HELLOWORLD_SCENE_H__
