#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
    auto layer = HelloWorld::create();
	PhysicsWorld* worald = scene->getPhysicsWorld();
	worald->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);
	scene->addChild(layer);
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
	circleNode1 = Node::create();
	circleNode2 = Node::create();
	circleNode3 = Node::create();
	circleNode4 = Node::create();
	addChild(circleNode1);
	addChild(circleNode2);
	addChild(circleNode3);
	addChild(circleNode4);

    return true;
}

void HelloWorld::onEnter(){
	Layer::onEnter();
	HelloWorld::makeTerrain();
	HelloWorld::experimental1();
}

void HelloWorld::makeTerrain(){
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	Vec2 terrain[7] = {
		Vec2(0, 100),
		Vec2(0, 0),
		Vec2(200, 0),
		Vec2(300, 50),
		Vec2(400, 50),
		Vec2(500, 0),
		Vec2(500, 100)
	};
	//Terrain 3���� �ۼ�.
	PhysicsBody* terrainBody1 = PhysicsBody::createEdgeChain(terrain, 7);
	Node* terrainNode1 = Node::create();
	terrainNode1->setPhysicsBody(terrainBody1);
	terrainNode1->setPosition(240, 500);
	addChild(terrainNode1);

	PhysicsBody* terrainBody2 = PhysicsBody::createEdgeChain(terrain, 7);
	Node* terrainNode2 = Node::create();
	terrainNode2->setPhysicsBody(terrainBody2);
	terrainNode2->setPosition(240, 350);
	addChild(terrainNode2);

	PhysicsBody* terrainBody3 = PhysicsBody::createEdgeChain(terrain, 7);
	Node* terrainNode3 = Node::create();
	terrainNode3->setPhysicsBody(terrainBody3);
	terrainNode3->setPosition(240, 200);
	addChild(terrainNode3);

	PhysicsBody* terrainBody4 = PhysicsBody::createEdgeChain(terrain, 7);
	Node* terrainNode4 = Node::create();
	terrainNode4->setPhysicsBody(terrainBody4);
	terrainNode4->setPosition(240, 50);
	addChild(terrainNode4);
}
void HelloWorld::makeCircle(){
	PhysicsWorld* circleBody1 = 
}
void HelloWorld::experimental1(){
	PhysicsBody* circleBody1 = circleNode1->getPhysicsBody();
	PhysicsBody* circleBody2 = circleNode2->getPhysicsBody();
	PhysicsBody* circleBody3 = circleNode3->getPhysicsBody();
	PhysicsBody* circleBody4 = circleNode4->getPhysicsBody();

	schedule([=](float dt)->void{
		circleBody1->applyImpulse(Vec2(2000,0));
		circleBody2->applyImpulse(Vec2(2000,0));
		circleBody3->applyImpulse(Vec2(2000,0));
		circleBody4->applyImpulse(Vec2(2000,0));
	}, 3.0f, "once");
}
void HelloWorld::experimental2(){
}				
void HelloWorld::experimental3(){
}
void HelloWorld::experimental4(){
}