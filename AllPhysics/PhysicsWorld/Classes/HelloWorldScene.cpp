#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
	auto scene = Scene::createWithPhysics();
	PhysicsWorld* world = scene->getPhysicsWorld();

	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);
    
	//init를 씬은 지금여기서 실행되고 있다. 
    auto layer = HelloWorld::create();
    scene->addChild(layer);
	layer->initPhysics();
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() ) return false;
    return true;
}

void HelloWorld::initPhysics(){
	PhysicsBody* physicsBody = PhysicsBody::createBox(Size(40, 40),
		PhysicsMaterial(0.1f, 1.0f, 0.0f));
	physicsBody->setDynamic(false);

	Sprite* node = Sprite::create("bird.png");
	node->setPosition(480, 320);
	addChild(node);
	node->setPhysicsBody(physicsBody);

	PhysicsWorld* Scene = getScene()->getPhysicsWorld();
	Scene->setGravity(Vec2(0,-500.0f));

	Node* wall = Node::create();
	wall->setPhysicsBody(PhysicsBody::createEdgeBox(Size(600, 500)));
	wall->setPosition(Vec2(480, 320));
	addChild(wall);

	for (int i = 0; i < 5; i++){
		physicsBody = PhysicsBody::createBox(Size(40, 40), PhysicsMaterial(0.1f, 1.0f, 0.0f));
		physicsBody->setGravityEnable(true);
		physicsBody->setVelocity(Vec2(random(-500.0f, 500.0f), random(-500.0f, 500.0f)));

		Sprite* sprite = Sprite::create("bird.png");
		sprite->setPosition(Vec2(random(0.0f, 600.0f), random(0.0f, 500.0f)));
		sprite->setPhysicsBody(physicsBody);
		addChild(sprite);
	}

}