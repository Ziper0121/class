#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	cocos2d::DrawNode* ray;
	float rayAngle = 0;
	float rayLength = 500;
	cocos2d::Vec2 target;
	cocos2d::Vec2 origin;

	void onEnter();
	bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	void logic(float);
	bool rayfunc(cocos2d::PhysicsWorld&, const cocos2d::PhysicsRayCastInfo&, void* );

};

#endif // __HELLOWORLD_SCENE_H__
