#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
	ray = DrawNode::create(2.0f);
	addChild(ray);

	auto touch = EventListenerTouchOneByOne::create();
	touch->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touch, this);

	schedule(schedule_selector(HelloWorld::logic));

    return true;
}

void HelloWorld::onEnter(){
	Layer::onEnter();

	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
}
bool HelloWorld::onTouchBegan(cocos2d::Touch* t, cocos2d::Event* e){
	PhysicsBody* body = PhysicsBody::createBox(Size(40, 40));
	body->setDynamic(false);

	Node* node = Node::create();
	node->setPosition(t->getLocation());
	node->setPhysicsBody(body);

	addChild(node);
	return true;
}
void HelloWorld::logic(float dt){
	ray->clear();
	origin = Vec2(480, 320);
	target = origin + Vec2(cos(rayAngle), sin(rayAngle))*rayLength;

	rayAngle += dt / 5.0f;

	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->rayCast(CC_CALLBACK_3(HelloWorld::rayfunc, this), origin, target, nullptr);
	ray->drawLine(origin, target, Color4F(0.0f, 1.0f, 0.0f, 1.0f));
}
bool HelloWorld::rayfunc(PhysicsWorld& world, const cocos2d::PhysicsRayCastInfo& info, void* data){
	ray->drawDot(info.contact, 4.0f, Color4F(0.0f, 1.0f, 0.0f, 1.0f));

		origin = info.contact;
		target = info.normal;

	return true;
}
