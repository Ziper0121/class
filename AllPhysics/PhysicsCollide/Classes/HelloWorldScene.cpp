#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

#define TAG_PLAYER 2
#define TAG_PLATFORM 1

using namespace cocostudio::timeline;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
	auto scene = Scene::createWithPhysics();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if ( !Layer::init() ) return false;
    
	/*auto collide = EventListenerPhysicsContact::create();
	collide->onContactBegin = std::bind(&HelloWorld::onPhysicsContacBegin, this, std::placeholders::_1);
	collide->onContactPreSolve = std::bind(&HelloWorld::onPhysicsContacPreSolve, this, std::placeholders::_1, std::placeholders::_2);
	collide->onContactPostSolve = std::bind(&HelloWorld::onPhysicsContacPostSolve, this, std::placeholders::_1, std::placeholders::_2);
	collide->onContactSeparate = std::bind(&HelloWorld::onPhysicsContactSeparate, this, std::placeholders::_1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(collide, this);
	*/

    return true;
}

void HelloWorld::onEnter(){
	Layer::onEnter();
	PhysicsWorld* world = getScene()->getPhysicsWorld();
	world->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	Node* stair2 = Node::create();
	PhysicsBody* stairBody = PhysicsBody::createBox(Size(200,30));
	stairBody->getShapes().at(0)->setTag(TAG_PLATFORM);
	stairBody->setContactTestBitmask(0xFFFFFFFF);
	stairBody->setDynamic(false);
	stair2->setPhysicsBody(stairBody);
	stair2->setPosition(480, 220);
	addChild(stair2);

	Node* player = Node::create();
	PhysicsBody* playerBody = PhysicsBody::createBox(Size(40, 40));
	playerBody->getShapes().at(0)->setTag(TAG_PLAYER);
	playerBody->setContactTestBitmask(0xFFFFFFFF);
	playerBody->setMass(10.0f);
	player->setPhysicsBody(playerBody);
	player->setPosition(480, 120);
	addChild(player);

	playerBody->applyImpulse(Vec2(0, 2000));
	auto collide = EventListenerPhysicsContact::create();
	collide->onContactBegin = std::bind(&HelloWorld::onPhysicsContacBegin, this, std::placeholders::_1);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(collide, this);

	/*Node* node = Node::create();
	PhysicsBody* wall = PhysicsBody::createEdgeBox(Size(500, 500));
	wall->setContactTestBitmask(0xFFFFFFFF);
	node->setPhysicsBody(wall);
	node->setPosition(480, 320);

	addChild(node);

	makeRectangle(Vec2(480, 300));*/
}

void HelloWorld::makeRectangle(cocos2d::Vec2 pos){
	Node* node = Node::create();

	PhysicsBody* body = PhysicsBody::createBox(Size(40, 40));
	node->setPhysicsBody(body);
	body->setContactTestBitmask(0xFFFFFFFF);

	node->setPosition(pos);
	addChild(node);
}

bool HelloWorld::onPhysicsContacBegin(cocos2d::PhysicsContact& contact){
	//CCLOG("onPhysicsContactBegin");
	int tagA = contact.getShapeA()->getTag();
	int tagB = contact.getShapeB()->getTag();

	//B를 기준으로 A 의 속도 방향,

	const PhysicsContactData* data = contact.getContactData();
	if (tagA == TAG_PLAYER && tagB == TAG_PLATFORM){
		CCLOG("A : PLAYER,B : PLATFORM");
		CCLOG("normal : (%f,%f)", data->normal.x, data->normal.y);
		// A가 플레이어일 경우 A의 아래가  B(플레이어)와 접촉방향일 경우 true 리턴,
		// (true 리턴시 접촉 감지 활성 | ㄹ민ㄷ flxjstl wjqchr fnxls antl)
		return data->normal.y < 0;
	}
	if (tagA == TAG_PLATFORM && tagB == TAG_PLAYER){
		CCLOG("A : PLAYER,B : PLATFORM");
		CCLOG("normal : (%f,%f)", data->normal.x, data->normal.y);
		// A가 플랫폼일 경우. A의 위가  B(플레이어)와 접촉방향일 경우 true 리턴,
		// (true 리턴시 접촉 감지 활성 | ㄹ민ㄷ flxjstl wjqchr fnxls antl)
		return data->normal.y > 0;
	}
	return true;
}
bool HelloWorld::onPhysicsContacPreSolve(cocos2d::PhysicsContact& contact, const cocos2d::PhysicsContactPreSolve&){
	CCLOG("onPhysicsContactPreSolve");
	return true;
}
void HelloWorld::onPhysicsContacPostSolve(cocos2d::PhysicsContact& contact, const cocos2d::PhysicsContactPostSolve&){
	CCLOG("onPhysicsContactPostSolve");
}
void HelloWorld::onPhysicsContactSeparate(cocos2d::PhysicsContact& contact){
	CCLOG("onPhysicsContactSeparate");
}